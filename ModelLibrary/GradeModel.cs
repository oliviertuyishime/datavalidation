﻿using System;
namespace ModelLibrary
{
    public class GradeModel
    {
        public string Course { get; set; }
        public double Grade { get; set; }
    }
}
