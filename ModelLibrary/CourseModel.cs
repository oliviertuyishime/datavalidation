﻿using System;
namespace ModelLibrary
{
    public class CourseModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
