﻿namespace ModelLibrary
{
    public class StudentModel
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string Level { get; set; }
    }
}
